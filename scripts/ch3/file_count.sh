#!/bin/bash
file_count() {
     DIR=$1
     local NUMBER_OF_FILES=$(ls ${DIR}| wc -l)
     echo "Directory = ${DIR}:"
     echo "    Number of files: ${NUMBER_OF_FILES}"
}

file_count $1
