#!/bin/bash
backup_file() {
    FILE_NAME=$1
    echo "Backing up ${FILE_NAME} with base name $(basename $FILE_NAME)"
    if [ -e $FILE_NAME ]
    then
        local BK_FILE_NAME="/Users/tan.nguyen/Documents/git_repo/shell_script/scripts/ch3/$(basename $FILE_NAME).$(date +%F).$$"
	echo "Back up file name ${BK_FILE_NAME}"
        cp $FILE_NAME $BK_FILE_NAME
    else
        return 1
    fi
}

backup_file /etc/hosts

if [ $? -eq "0" ]
then
    echo "Back up was successful"
else
    echo "Back up failed"
    exit 1
fi
