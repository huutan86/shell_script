#!/bin/bash
FILENAME="/etc/shadow"
if [ -e ${FILENAME} ]
then
    echo "Shadow passwords are enabled."
    if [ -w ${FILENAME} ]
    then
        echo "You have permission to write to the file"
    else
	echo "You do not have permission to write to the file."
    fi
fi
