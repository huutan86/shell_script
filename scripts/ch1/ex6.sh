#!/bin/bash
read -p "Enter the name of the file: " FILENAME
if [ -f $FILENAME ]
then
    echo "${FILENAME} is a regular file!"
elif [ -d $FILENAME ]
then
    echo "${FILENAME} is a directory."
else
    echo "${FILENAME} is another type of file."
fi

ls -l $FILENAME
