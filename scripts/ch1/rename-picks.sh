#!/bin/bash
PICTURES=$(ls *.png)
DATE=$(date +%F)
for PICTURE in $PICTURES
do
    echo "renaming picture ${PICTURE} to ${DATE}-${PICTURE}"
    mv ${PICTURE} ${DATE}-${PICTURE}
done
