#!/bin/bash
echo "Running the script $1"
FILENAME=$1
if [ -f $FILENAME ]
then
    echo "${FILENAME} is a regular file!"
elif [ -d $FILENAME ]
then
    echo "${FILENAME} is a directory."
else
    echo "${FILENAME} is another type of file."
fi

ls -l $FILENAME
