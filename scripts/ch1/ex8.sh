#!/bin/bash
echo "Running the script $1"
for FILENAME in $@
do
    if [ -f $FILENAME ]
    then
        echo "${FILENAME} is a regular file!"
    elif [ -d $FILENAME ]
    then
        echo "${FILENAME} is a directory."
    else
        echo "${FILENAME} is another type of file."
    fi

    ls -l $FILENAME
done
