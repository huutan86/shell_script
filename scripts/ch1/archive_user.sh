#!/bin/bash

read -p "Enter a user name: " USER

# USER=$1   # The first parameter is the user.
echo "executing script: $0"
echo "Archiving user ${USER}"
for USER in $@
do
   echo "archiving user: $USER"

   # Lock the password
   passwd -l $USER

   # Create an archive of the home directory
   tar cf ./${USER}.tar.gz /home/${USER}
done
