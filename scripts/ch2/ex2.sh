#!/bin/bash
echo "Executing the script $0"
NAME=$1
if [ -f $NAME ]
then
    exit 0
elif [ -d $NAME ]
then
    exit 1
else
    exit 2
fi
